#!/usr/bin/python

import sys
#import logging
import os
#logging.basicConfig(stream=sys.stderr)

#sys.path.insert(0,"/var/www/mp3site")
this_dir = os.path.dirname(__file__)
sys.path.insert(0, this_dir)
# Now that the Python path includes the current directory, any
# application specific modules can be loaded just like this was
# in main.py.


# Now that the Python path includes the current directory, any
# application specific modules can be loaded just like this was
# in main.py.

from app import app as application
#app.debug = True
#from werkzeug.debug import DebuggedApplication 
#application = DebuggedApplication(app, True)

if not application.debug:
	import logging
	this_dir = os.path.dirname(__file__)
	log_file = os.path.join(this_dir, 'production.log')
	file_handler = logging.FileHandler(log_file)
	file_handler.setLevel(logging.WARNING)
	application.logger.addHandler(file_handler)
