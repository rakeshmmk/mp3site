from app import db

class Site(db.Model):
        __tablename__ = 'site'
        id      = db.Column(db.Integer, db.Sequence('site_id_seq'), primary_key=True)
        name    = db.Column(db.String(12))
        url     = db.Column(db.String(200))

        #files   = db.relationship("File", order_by="File.id", backref="site")
        #songs   = db.relationship("Song", order_by="Song.id", backref="site")
        #movies   = db.relationship("Movie", order_by="Movie.id", backref="site")

        def __repr__(self):
                return "<Site(name='%s', url='%s')>" % (
                        self.name, self.url)



class Movie(db.Model):
        __tablename__ = 'movie'
        id      = db.Column(db.Integer, db.Sequence('mov_id_seq'), primary_key=True)
        name    = db.Column(db.String(12))
        url     = db.Column(db.String(200))

        site_id = db.Column(db.Integer, db.ForeignKey('site.id'))
        site   = db.relationship("Site")
        #files   = db.relationship("File", order_by="File.id", backref="movie")
        #songs   = db.relationship("Song", order_by="Song.id", backref="movie")

        def __repr__(self):
                return "<Movie(name='%s', url='%s',site_id='%s')>" % (
                        self.name, self.url,self.site_id)

class Song(db.Model):
        __tablename__ = 'song'
        id      = db.Column(db.Integer, db.Sequence('song_id_seq'), primary_key=True)
        name    = db.Column(db.String(12))
        url     = db.Column(db.String(200))
        #Foriegn keys
        mov_id  = db.Column(db.Integer, db.ForeignKey('movie.id'))
        site_id = db.Column(db.Integer, db.ForeignKey('site.id'))

        movie  = db.relationship("Movie")
        site   = db.relationship("Site")

class File(db.Model):
        __tablename__ = 'file'
        id      = db.Column(db.Integer, db.Sequence('file_id_seq'), primary_key=True)
        name    = db.Column(db.String(100))
        url     = db.Column(db.String(200))

        ##Foreign keys
        mov_id  = db.Column(db.Integer, db.ForeignKey('movie.id'))
        song_id = db.Column(db.Integer, db.ForeignKey('song.id'))
        site_id = db.Column(db.Integer, db.ForeignKey('site.id'))

        #Relationships
        movie   = db.relationship("Movie")
        site    = db.relationship("Site")
        song    = db.relationship("Song")

        #songs          = db.relationship("Songs", backref=backref('files', order_by=id))
        #movies  = db.relationship("Movies", backref=backref('files', order_by=id))
        #sites          = db.relationship("Sites", backref=backref('files', order_by=id))


        def __repr__(self):
                return "<File(name='%s', url='%s', mov_id='%s',song_id='%s',site_id='%s')>" % (
                        self.name, self.url,self.mov_id,self.song_id,self.site_id)
