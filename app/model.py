from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime,TIMESTAMP,BOOLEAN
from sqlalchemy import Sequence,ForeignKey,func
from sqlalchemy.orm import relationship, backref
#from msmusiq_crawler import Base
import datetime
from sqlalchemy import create_engine

Base = declarative_base()

##Define the types of tages
TAG_MOVIE	= 1
TAG_SONG	= 2
TAG_UNKNOWN	= 3
TAG_TITLE	= 4
TAG_ALBUM	= 5 
TAG_ARTIST	= 6
TAG_ANCHOR_TEXT	= 7

##List of websites
SITE_MALLUDEVIL		= 'malludevil.in'
SITE_MSMUSIQ		= 'msmusiq.com'	
SITE_ORUWEBSITE		= 'db.oruwebsite.com'
SITE_WAPMALAYALAM	= 'wap-malayalam.wap-ka.com'
##ORM to store file details                      

class File(Base):
	__tablename__ 	= 'file'
	id 		= Column(Integer, Sequence('file_id_seq'), primary_key=True)
	name 		= Column(String(100))
	#url 		= Column(String(200),index=True,unique=True)
	url 		= Column(String(200),index=True,unique=True)
	landing_url	= Column(String(200))
	direct_url	= Column(BOOLEAN)
	site		= Column(String(100))
        file_size 	= Column(String(50))
	file_bitrate	= Column(String(10))
	file_time	= Column(String(10))
        created_date    = Column(TIMESTAMP, server_default=func.now())
        last_updated    = Column(TIMESTAMP, server_default=func.now(), onupdate=func.current_timestamp())
#	created_date 	= Column(DateTime, default=datetime.datetime.utcnow)
#	tags   		= relationship("FileTag", order_by="FileTag.id", backref="file")


        def __repr__(self):
                return "<File(name='%s', url='%s', landing_url='%s',direct_url='%s',site='%s',file_size='%s',file_bitrate='%s',file_time='%s',created_date='%s',last_updated='%s')>" % (
                        self.name, self.url,self.landing_url,self.direct_url,self.site,self.file_size,self.file_bitrate,self.file_time,self.created_date,self.last_updated)
class FileTag(Base):
	__tablename__	= "file_tag"
	id              = Column(Integer, Sequence('filetag_id_seq'), primary_key=True)
	tag		= Column(String(200))
	file_id 	= Column(Integer, ForeignKey('file.id'))
	fle   		= relationship("File")
	tag_type	= Column(Integer)

	def __repr__(self):
		return "<FileTag(tag='%s',file_id='%s',tag_type='%s')>" % (
			self.tag,self.file_id,self.tage_type)


class Query(Base):
	__tablename__	= "query"
        id              = Column(Integer, Sequence('query_id_seq'), primary_key=True)
	query_string	= Column(String(50),index=True)
	created_date    = Column(TIMESTAMP, server_default=func.now())
	last_updated	= Column(TIMESTAMP, server_default=func.now(), onupdate=func.current_timestamp())
	num_queries	= Column(Integer)

	def __repr__(self):
		return "<Query(query_string='%s',created_date='%s',last_updated='%s')>" %s(
			self.query_string,self.created_date,self.last_updated)


###SQLalchemy ORM declarations END#################################


if __name__ == "__main__":

	# Create an engine that stores data in the local directory's
	# sqlalchemy_example.db file.
	#engine = create_engine('sqlite:///database.db')
	engine = create_engine('mysql://root:greatuser@localhost/crawler?charset=utf8', convert_unicode=True)

	# Create all tables in the engine. This is equivalent to "Create Table"
	# statements in raw SQL.
	Base.metadata.create_all(engine)

