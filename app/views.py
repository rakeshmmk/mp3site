from app import app
from flask import render_template,flash,request,url_for,redirect,make_response
from forms import SearchForm
from app.database import db_session
from app.model import File,Query,FileTag
from sphinxapi import SphinxClient
import re
import urllib

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/')
@app.route('/index')
def index():
	
	return render_template("home.html")

@app.route('/search/')
def search():

	querry 	= request.args.get('q',"")

	songs	= []
	text	= ""
	if querry:
		#remove all special chara excet +
        	#convert + to _
	        querry_nosp     = re.sub('[^a-zA-Z0-9\+]', ' ', querry)
        	querry_url      = re.sub('[\+\s+]+', '_', querry_nosp)+".html"

	        return redirect(url_for('html_search',search_string=querry_url))
	
		
        return render_template("search.html",querry=querry)

@app.route('/sendme/<path:path>')
def sendme(path):
	##Redirect to a webpage without revealing the refferer
	return render_template("redirect.html",url=path)


@app.route('/mp3/<search_string>')
def html_search(search_string):
	#return "OKKK"

	html_check	= re.compile("(.+)\.html$")
	check	=  re.search(html_check,search_string)
	if check:
		#querry	= check.group(1)
		querry     = re.sub('[^a-zA-Z0-9]', ' ',check.group(1))
		###Code to keep track for queries start
		###TODO add code to identify searches without any results!!
		old_query		= db_session.query(Query).filter(Query.query_string == querry).first()
		if old_query:
			print "Query: "+querry+ " have queried "+str(old_query.num_queries)+" times already!! Updating the count"
			old_query.num_queries = old_query.num_queries + 1
			db_session.add(old_query)
			db_session.commit()
		else:

			new_query	= Query(query_string=querry,num_queries=1)
                        db_session.add(new_query)
                        db_session.commit()
                ###Code to keep track for queries end
	
		###Remove commonly used words from query
		rgxcommon   = re.compile('mp3|song|download|songs|downloads|free|video|hd|melody|remix',re.I)
		safequerry     = re.sub(rgxcommon, '',querry)
		safequerry     = re.sub(re.compile('^\s+'), '',safequerry)
		safequerry     = re.sub(re.compile('\s+$'), '',safequerry)
		##Connect to sphinx server and qury		
                client = SphinxClient()
                client.SetServer('127.0.0.1', 9312)
                results = client.Query(safequerry)
                        
	 	songs   = []
		if results:
			if results.get('matches'): ## Search resulted in matches
				for result in results['matches']:
					song            = {}
					(song['id'],song['tag'])   = db_session.query(FileTag.file_id,FileTag.tag).filter(FileTag.id == result['id'] ).one()
					(song['name'],song['url']) = db_session.query(File.name,File.url).filter(File.id == song['id'] ).one()
				#	song['url']	= urllib.quote(song['url'])
					songs.append(song)
				topSearches	= getTopSearches()
				newMusic	= getFreshMusic()
				return render_template("search.html",querry=querry,songs=songs,topSearches=topSearches,newMusic=newMusic)
	
	                else:  #No matches found!!

        	                return render_template("search.html",querry=querry)


		else:  #No matches found!!

			return render_template("search.html",querry=querry)


	##TODO add some nice erro page!

	else:
		return "ERROR"
	
# a route for generating sitemap.xml
@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
	pages			= []
	query_db               	= db_session.query(Query)
	for query in query_db:
		search_string	= re.sub('[\+\s+]+', '_',query.query_string)+".html"
		url 		= "http://mp3tikka.com" + url_for('html_search',search_string=search_string)
		modified_time	= query.last_updated.date().isoformat()
		pages.append([url,modified_time]) 
	sitemap_xml 	= render_template('sitemap_template.xml', pages=pages)
      	response	= make_response(sitemap_xml)
      	response.headers["Content-Type"] = "application/xml"    
    
      	return response

##get top posts from query table
def getTopSearches():
	searches		= []
	queries			= db_session.query(Query).order_by(Query.num_queries.desc()).limit(10)
	for query in queries:
		search_string   = re.sub('[\+\s+]+', '_',query.query_string)+".html"
                url             = "http://mp3tikka.com" + url_for('html_search',search_string=search_string)
		searches.append({'search_string':query.query_string,'url':url})

#	print searches
	return searches

##Get Newly added tracks
def getFreshMusic():
        searches                = []
        queries                 = db_session.query(Query).order_by(Query.last_updated.desc()).limit(10)
        for query in queries:
                search_string   = re.sub('[\+\s+]+', '_',query.query_string)+".html"
                url             = "http://mp3tikka.com" + url_for('html_search',search_string=search_string)
                searches.append({'search_string':query.query_string,'url':url})

 #       print searches
        return searches

